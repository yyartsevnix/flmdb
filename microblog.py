from app import app
import os

os.environ.update({'FLASK_APP':'microblog.py'})

if __name__ == '__main__':
    app.run()
