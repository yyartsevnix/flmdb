# -*- coding: utf-8 -*-
from flask import render_template, flash, redirect, request, url_for
from app import app
from .forms import LoginForm, TextNumForm


@app.route("/")
@app.route("/index")
def index():
    endpoints = [
        {
            'Name': 'Index (this page)',
            'url': url_for('index')
        },
        {
            'Name': 'Login',
            'url': url_for('login')
        },
        {
            'Name': 'Get form',
            'url': url_for('get_form')
        },
        {
            'Name': 'Get response',
            'url': url_for('get_response')
        },
        {
            'Name': 'Post form',
            'url': url_for('post_form')
        },
        {
            'Name': 'Post response',
            'url': url_for('post_response')
        },
        {
            'Name': 'Logout',
            'url': url_for('logout')
        },
    ]
    return render_template('index.html', title='Home', endpoints=endpoints)


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        flash(f'Login requested for user {form.username.data}, remember_me = {form.remember_me.data}')
        return redirect('/index')
    return render_template('login.html', title='Sign In', form=form)


@app.route('/get_form')
def get_form():
    form = TextNumForm()
    return render_template('get_form.html', form=form)


@app.route('/post_form')
def post_form():
    form = TextNumForm()
    return render_template('post_form.html', form=form)


@app.route('/get_response')
def get_response():
    text = request.args.get('text')
    if not text:
        text = 'None'
    number = request.args.get('number')
    if not number:
        number = 'None'
    return f'Text:{text}<br/>Number:{number}'


@app.route('/post_response', methods=['POST'])
def post_response():
    text = request.args.get('text')
    number = request.args.get('number')
    return f'Text:{text}<br/>Number:{number}'


@app.route('/logout')
def logout():
    flash('Logged out')
    return redirect('/index')
